local fn = vim.fn
local gkey = vim.api.nvim_set_var


-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]]

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  return
end


-- Install your plugins here
return packer.startup(function(use)
  -- My plugins here
  -- standard stuff dunno
  use "wbthomason/packer.nvim" -- Have packer manage itself
  use "nvim-lua/popup.nvim" -- idk but i guess
  use "nvim-lua/plenary.nvim" -- Useful lua functions used by lots of plugins
  --colors
  use "rebelot/kanagawa.nvim"
  --use "sainnhe/everforest"
  -- lsp 
  use 'neovim/nvim-lspconfig' --configurations for lsp 

  --cmp et al.
  use "hrsh7th/cmp-nvim-lsp" -- also from nvim cmp-repo
  use "hrsh7th/cmp-buffer" -- buffer completions
  use "hrsh7th/cmp-path" -- path completions
  use "hrsh7th/cmp-cmdline" -- cmdline completions
  use "f3fora/cmp-spell"
  use "hrsh7th/nvim-cmp" -- The completion plugin
--  --
--  -- snippets (via luasnip)
  -- treesitter
  use {
    "nvim-treesitter/nvim-treesitter",
    run = ":TSUpdate",
  }

  use { 
    'martineausimon/nvim-lilypond-suite',
    requires = 'MunifTanjim/nui.nvim',
    config = function()
      require('nvls').setup({
        -- edit config here (see "Customize default settings" in wiki)
      })
    end
  }

  use { "nvim-telescope/telescope-bibtex.nvim",
    requires = {
      {'nvim-telescope/telescope.nvim'},
    },
    config = function ()
      require"telescope".load_extension("bibtex")
    end,
  }
  if PACKER_BOOTSTRAP then
    require("packer").sync()
  end
end)



