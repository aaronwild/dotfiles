
vim.g.vimtex_view_method = 'zathura'
-- set lualatex as compiler
vim.g.vimtex_compiler_engine = 'lualatex'
vim.g.vimtex_quickfix_ignore_filters = { 'hyperref' , 'gitinfo', 'You have requested'}
