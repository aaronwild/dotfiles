require "ruth.options"
require "ruth.plugins"
require "ruth.lsp"
require "ruth.treesitter"
--require "ruth.luasnip"
require "ruth.completion"
require "ruth.keymaps"
vim.cmd "colorscheme kanagawa"
--vim.cmd "set background=light"
vim.opt.spell = false 
vim.opt.spelllang = { 'en_us' }
